require "json"
SOURCE_ADRESS = "data.json"

def initialize_data
  raw_file = File.read(SOURCE_ADRESS)
  data = JSON.parse(raw_file)
  data.each do |lang|
    raw_tags = lang.values.join(' ').delete(',')
    lang["Tags"] = raw_tags.split(' ').map{|t| t.downcase} # Create Tags field for each language from every word in every field
  end
end

def simple_searsh(target_data, query)
  query.map!{|t| t.downcase}
  target_data.select{|lang| (query-lang["Tags"]).empty?} # Go through all languages and check if current language has all query tags	
end

def exact_search(target_data, exact_query)
  target_data.select do |lang| # Go through all languages and...
  	raw_values = lang.select{|k,v| k != "Tags"}.values
    exact_query.all?{|query_string| raw_values.select{|v| v.include?(query_string)}.any?} #...and check if the language fields contain exactstring from a exact query  
  end
end

def negative_search(target_data, query)
  query.map!{|t| t.downcase}
  target_data.select{|lang| (query & lang["Tags"]).empty?} # Go through all languages and check if current language don't have any query tags	
end

s_index = ARGV.index('-s')
n_index = ARGV.index('-n')
query = ARGV[0...s_index]
strict_query = ARGV[s_index+1...n_index] if s_index
negative_query = ARGV[n_index+1..-1] if n_index

data = initialize_data
searched_languages = simple_searsh(data, query)
searched_languages = exact_search(searched_languages, strict_query) if s_index
searched_languages = negative_search(searched_languages, negative_query) if n_index
p searched_languages.map{|lang| lang["Name"]}
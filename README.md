
## Test task

This script can do searching in 3 ways: by words, strict search and negative search. Script can be easily used without any changes to search by other fields, not only Name, Type and Designed by. Was tested on ruby 2.6.3p62.

### Search by word
example: 

	ruby search.rb Scripting Microsoft 
will give us ["JScript", "VBScript", "Windows PowerShell"]

### Strict search
example: 

	ruby search.rb Interpreted -s "Thomas Eugene" 
will give us ["BASIC"]

	ruby search.rb -s "Dan Ingalls" "Adele Goldberg" 
will give us ["Smalltalk"]

Important note: strict search is case sensetive.
### Negative search
example: 

	ruby search.rb john -s -n array 
will give us ["BASIC", "Haskell", "Lisp", "S-Lang"]

	ruby search.rb john -s "E. Davis" -n array 
will give us ["S-Lang"]

Important note: if we use "-n" flag, "-s" must be present and written before "-n". 
